#ifndef SPIKES_H
# define SPIKES_H

# include "elements.h"
# include "engine.h"

ENGINE_DEF_AGENT(spikes, 0, 1, GLOOP_UPDATE | GLOOP_RIGID | GLOOP_COLLISIONS,
                 SPIKES)
struct spikes
{
    ENGINE_AGENT(spikes);

    int xspeed;
};
ENGINE_DEF_MAKE(spikes)

void spikes_init(struct spikes *spikes);

void spikes_collision(struct spikes *spikes, struct agent *collider);

void spikes_update(struct spikes *spikes, double delta);

void spikes_did_enter_scene(struct spikes *spikes);

void spikes_draw(struct spikes *spikes);

void spikes_destroy(struct spikes *spikes);

#endif /* !SPIKES_H */
