#ifndef HEALTH_BAR_H
#define HEALTH_BAR_H

#include <SDL2/SDL.h>

void draw_health_bar(SDL_Rect bar, double health);

#endif /* !HEALTH_BAR_H */
