#ifndef EXIT_H
# define EXIT_H

# include "elements.h"
# include "engine.h"

ENGINE_DEF_AGENT(exit, 0, 1, GLOOP_COLLISIONS, EXIT)
struct exit
{
    ENGINE_AGENT(exit);

    int pos;
};
ENGINE_DEF_MAKE(exit)

void exit_init(struct exit *exit);

void exit_collision(struct exit *exit, struct agent *collider);

void exit_update(struct exit *exit, double delta);

void exit_did_enter_scene(struct exit *exit);

void exit_draw(struct exit *exit);

void exit_destroy(struct exit *exit);

#endif /* !EXIT_H */
