#ifndef DEMON_H
# define DEMON_H

# include "elements.h"
# include "engine.h"

# define COOLDOWN 120

ENGINE_DEF_AGENT(demon, 0, 1, GLOOP_UPDATE | GLOOP_COLLISIONS | GLOOP_RIGID,
                 DEMON)
struct demon
{
    ENGINE_AGENT(demon);

    int xspeed;
    int yspeed;
    int direction;

    int shoot;

    unsigned damaged;
    int hp;
};
ENGINE_DEF_MAKE(demon)

void demon_init(struct demon *demon);

void demon_collision(struct demon *demon, struct agent *collider);

void demon_update(struct demon *demon, double delta);

void demon_did_enter_scene(struct demon *demon);

void demon_draw(struct demon *demon);

void demon_destroy(struct demon *demon);

#endif /* !DEMON_H */
