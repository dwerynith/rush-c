#ifndef VECTOR_H
#define VECTOR_H

#include <stdlib.h>
#include <string.h>

#define DECL_VECTOR_STRUCT(Name, Type)          \
  struct Name                                   \
  {                                             \
    size_t size;                                \
    size_t capacity;                            \
    Type *data;                                 \
  };

#define DECL_VECTOR_INIT_SIZE 16
#define DECL_VECTOR_INIT(Name, Type)                                    \
  static inline struct Name *Name ## _make(void)                        \
  {                                                                     \
    struct Name *vector = malloc(sizeof(*vector));                      \
    if (!vector)                                                        \
      abort();                                                          \
    vector->data = calloc(DECL_VECTOR_INIT_SIZE, sizeof(Type));         \
    if (!vector->data)                                                  \
      abort();                                                          \
    vector->size = 0;                                                   \
    vector->capacity = DECL_VECTOR_INIT_SIZE;                           \
    return vector;                                                      \
  }

#define DECL_VECTOR_FREE(Name, Type)                                    \
  static inline void Name ## _free(struct Name *vector, int free_data)  \
  {                                                                     \
    if (free_data)                                                      \
      free(vector->data);                                               \
    vector->capacity = 0;                                               \
    vector->size = 0;                                                   \
    free(vector);                                                       \
  }

#define DECL_VECTOR_SIZE(Name, Type)                            \
  static inline size_t Name ## _size(const struct Name *vector) \
  {                                                             \
    return vector->size;                                        \
  }

#define DECL_VECTOR_INSERT(Name, Type)                          \
  static inline int Name ## _insert(struct Name *vector,        \
                      size_t pos, Type element)                 \
  {                                                             \
    if (vector->size == vector->capacity)                       \
    {                                                           \
      Type *tmp = realloc(vector->data, 2 * vector->capacity    \
                          * sizeof(Type));                      \
      if (!tmp)                                                 \
        return 0;                                               \
                                                                \
      vector->data = tmp;                                       \
      vector->capacity *= 2;                                    \
    }                                                           \
    memmove(vector->data + pos + 1, vector->data + pos,         \
            sizeof(Type) * (vector->size - pos));               \
    vector->data[pos] = element;                                \
    ++vector->size;                                             \
    return 1;                                                   \
  }

#define DECL_VECTOR_PUSH_BACK(Name, Type)                       \
  static inline int Name ## _push_back(struct Name *vector,     \
                         Type element)                          \
  {                                                             \
    if (vector->size == vector->capacity)                       \
    {                                                           \
      Type *tmp = realloc(vector->data, 2 * vector->capacity    \
                          * sizeof(Type));                      \
      if (!tmp)                                                 \
        return 0;                                               \
                                                                \
      vector->data = tmp;                                       \
      vector->capacity *= 2;                                    \
    }                                                           \
    vector->data[vector->size++] = element;                     \
    return 1;                                                   \
  }

#define DECL_VECTOR_REMOVE(Name, Type)                                  \
  static inline int Name ## _remove(struct Name *vector,                \
                      size_t position)                                  \
  {                                                                     \
    memmove(vector->data + position, vector->data + position + 1,       \
            sizeof(Type) * (vector->size - position - 1));              \
    --vector->size;                                                     \
                                                                        \
    if (vector->size < vector->capacity / 2)                            \
    {                                                                   \
      Type *tmp = realloc(vector->data, (vector->capacity / 2)          \
                          * sizeof(Type));                              \
      if (!tmp)                                                         \
        return 0;                                                       \
                                                                        \
      vector->data = tmp;                                               \
      vector->capacity /= 2;                                            \
    }                                                                   \
    return 1;                                                           \
  }

#define DECL_VECTOR_GET(Name, Type)                                     \
  static inline Type Name ## _get(struct Name *vector, size_t position) \
  {                                                                     \
    return vector->data[position];                                      \
  }

#define DECL_VECTOR(Name, Type)                 \
  DECL_VECTOR_STRUCT(Name, Type)                \
  DECL_VECTOR_INIT(Name, Type)                  \
  DECL_VECTOR_FREE(Name, Type)                  \
  DECL_VECTOR_SIZE(Name, Type)                  \
  DECL_VECTOR_INSERT(Name, Type)                \
  DECL_VECTOR_PUSH_BACK(Name, Type)             \
  DECL_VECTOR_REMOVE(Name, Type)                \
  DECL_VECTOR_GET(Name, Type)

#endif /* !VECTOR_H */
