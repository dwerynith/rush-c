#ifndef PLAYER_H_
# define PLAYER_H_

#include "elements.h"
#include "engine.h"

ENGINE_DEF_AGENT(player, 1, 2, GLOOP_COLLISIONS | GLOOP_UPDATE | GLOOP_RIGID,
                 PLAYER)
struct player
{
    ENGINE_AGENT(player);

    float xspeed;
    float yspeed;
    int direction;

    float gravity;
    int on_ground;
    int double_jump;

    int air_cd;
    int cooldown;

    int health;
    int invincibility;

    int attacking;
    int img_start;
    int frame;

    int isgod;
    int gmcd;

    int can_move;
};
ENGINE_DEF_MAKE(player)

void player_init(struct player *player);

void player_collision(struct player *player, struct agent *collider);

void player_update(struct player *player, double delta);

void player_did_enter_scene(struct player *player);

void player_draw(struct player *player);

void player_destroy(struct player *player);

#endif /* !PLAYER_H_ */
