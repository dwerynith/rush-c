#ifndef PHOTO_H_
# define PHOTO_H_

# include "elements.h"
# include "engine.h"

#define P_WIDTH 90
#define P_HEIGHT 128
# define NB_PHOTO 15
# define PHOTO_TIME 120

ENGINE_DEF_AGENT(photo, 0, 1, GLOOP_UPDATE, PHOTO)
struct photo
{
    ENGINE_AGENT(photo);
    int id;
    int timeleft;
};
ENGINE_DEF_MAKE(photo)

void photo_init(struct photo *photo);

void photo_collision(struct photo *photo, struct agent *collider);

void photo_update(struct photo *photo, double delta);

void photo_did_enter_scene(struct photo *photo);

void photo_draw(struct photo *photo);

void photo_destroy(struct photo *photo);

#endif /* !PHOTO_H_ */
