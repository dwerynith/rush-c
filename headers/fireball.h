#ifndef FIREBALL_H
# define FIREBALL_H

# include "elements.h"
# include "engine.h"

ENGINE_DEF_AGENT(fireball, 0, 1, GLOOP_UPDATE | GLOOP_COLLISIONS | GLOOP_RIGID,
                 FIREBALL)
struct fireball
{
    ENGINE_AGENT(fireball);

    int xspeed;
    int yspeed;
};
ENGINE_DEF_MAKE(fireball)

void fireball_init(struct fireball *fireball);

void fireball_collision(struct fireball *fireball, struct agent *collider);

void fireball_update(struct fireball *fireball, double delta);

void fireball_did_enter_scene(struct fireball *fireball);

void fireball_draw(struct fireball *fireball);

void fireball_destroy(struct fireball *fireball);

#endif /* !FIREBALL_H */
