#ifndef ENGINE_H
#define ENGINE_H

#include <SDL2/SDL.h>

#include "vector.h"
#include "elements.h"

static inline
unsigned engine_uid_next(void)
{
  static unsigned uid = 0;
  return ++uid;
}

#define GLOOP_PASSIVE (0)
#define GLOOP_COLLISIONS (1 << 0)
#define GLOOP_UPDATE (1 << 1)
#define GLOOP_RIGID (1 << 2)

#define ENGINE_AGENT(Agent)                     \
  const struct Agent ## _meta *meta;            \
  struct Agent *self;                           \
  struct agent *next;                           \
  struct agent *prev;                           \
  unsigned uid;                                 \
  SDL_Rect rect;                                \
  int redraw

#define ENGINE_DEF_AGENT(Agent, Survives, Z, Flags, Elt)                \
  struct agent;                                                         \
  struct Agent;                                                         \
  void Agent ## _init(struct Agent *agent);                             \
  void Agent ## _collision(struct Agent *agent, struct agent *);        \
  void Agent ## _update(struct Agent *agent, double delta);             \
  void Agent ## _draw(struct Agent *agent);                             \
  void Agent ## _did_enter_scene(struct Agent *agent);                  \
  void Agent ## _destroy(struct Agent *agent);                          \
                                                                        \
  struct Agent ## _meta                                                 \
  {                                                                     \
    const char *agent_name;                                             \
    int survives_scene;                                                 \
    enum elements elt;                                                  \
                                                                        \
    unsigned zindex;                                                    \
    unsigned gloop_flags;                                               \
                                                                        \
    void (*init)(struct Agent *);                                       \
    void (*collision)(struct Agent *, struct agent *);                  \
    void (*update)(struct Agent *, double delta);                       \
    void (*draw)(struct Agent *);                                       \
    void (*did_enter_scene)(struct Agent *);                            \
    void (*destroy)(struct Agent *);                                    \
  };                                                                    \
                                                                        \
  static inline                                                         \
  const struct Agent ## _meta *Agent ## _meta_get(void)                 \
  {                                                                     \
    static struct Agent ## _meta meta =                                 \
    {                                                                   \
      .agent_name = "struct " #Agent,                                   \
      .survives_scene = Survives,                                       \
      .elt = Elt,                                                       \
                                                                        \
      .zindex = Z,                                                      \
      .gloop_flags = Flags,                                             \
                                                                        \
      .init = Agent ## _init,                                           \
      .collision = Agent ## _collision,                                 \
      .update = Agent ## _update,                                       \
      .draw = Agent ## _draw,                                           \
      .did_enter_scene = Agent ## _did_enter_scene,                     \
      .destroy = Agent ## _destroy,                                     \
    };                                                                  \
                                                                        \
    return &meta;                                                       \
  }

#define ENGINE_DEF_MAKE(Agent)                          \
  static inline                                         \
  struct Agent *Agent ## _make(void)                    \
  {                                                     \
    struct Agent *agent = malloc(sizeof(struct Agent)); \
    agent->uid = engine_uid_next();                     \
    agent->rect.x = 0;                                  \
    agent->rect.y = 0;                                  \
    agent->rect.w = 0;                                  \
    agent->rect.h = 0;                                  \
    agent->redraw = 1;                                  \
    agent->self = agent;                                \
    agent->meta = Agent ## _meta_get();                 \
    agent->next = NULL;                                 \
    agent->prev = NULL;                                 \
    return agent;                                       \
  }

ENGINE_DEF_AGENT(agent, 0, 0, GLOOP_PASSIVE, NONE)
struct agent
{
  ENGINE_AGENT(agent);
};

DECL_VECTOR(agent_vector, struct agent *)

struct scene
{
  SDL_Rect view;
  struct agent *first;

  SDL_Texture *bg_tex;

  SDL_Rect dim;

  void (*init)(struct scene *scene);
  void (*destroy)(struct scene *scene);
};

struct scene *engine_scene_make(void);

#define ENGINE_WIN_WIDTH 1280
#define ENGINE_WIN_HEIGHT 960
#define ENGINE_LOOKUP_SIZE ((ENGINE_WIN_HEIGHT + 1) * (ENGINE_WIN_WIDTH + 1))
struct engine
{
  int running;
  struct scene *scene;
  struct scene *next_scene;

  struct agent_vector *lookup[ENGINE_LOOKUP_SIZE];

  SDL_Window *window;
  SDL_Renderer *renderer;
};

struct engine *engine_get(void);

void engine_agent_add(struct agent *agent);

void engine_agent_remove(struct agent *agent);

long engine_agent_lookup_idx(struct agent *agent);

#endif /* !ENGINE_H */
