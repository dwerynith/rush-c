#ifndef SCENES_H_
# define SCENES_H_

# include "engine.h"

struct scene *game_over_scene_make(void);
void game_over_scene_destroy(struct scene *scene);
void game_over_scene_init(struct scene *scene);

void first_scene_init(struct scene *scene);
void first_scene_destroy(struct scene *scene);
struct scene *first_scene(void);

void level2_init(struct scene *scene);
void level2_destroy(struct scene *scene);
struct scene *level2_scene_make(void);

void boss_scene_init(struct scene *scene);
void boss_scene_destroy(struct scene *scene);
struct scene *boss_scene_make(void);

void credits_scene_init(struct scene *scene);
void credits_scene_destroy(struct scene *scene);
struct scene *credits_scene_make(void);

#endif /* !SCENES_H_ */
