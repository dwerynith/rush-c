#ifndef GOTO3_H
# define GOTO3_H

# include "elements.h"
# include "engine.h"

ENGINE_DEF_AGENT(goto3, 0, 1, GLOOP_COLLISIONS, GOTO3)
struct goto3
{
    ENGINE_AGENT(goto3);

    int pos;
};
ENGINE_DEF_MAKE(goto3)

void goto3_init(struct goto3 *goto3);

void goto3_collision(struct goto3 *goto3, struct agent *collider);

void goto3_update(struct goto3 *goto3, double delta);

void goto3_did_enter_scene(struct goto3 *goto3);

void goto3_draw(struct goto3 *goto3);

void goto3_destroy(struct goto3 *goto3);

#endif /* !GOTO3_H */
