#ifndef SCORE_H
# define SCORE_H

# include "photos.h"

struct score
{
  int photos[NB_PHOTO];
};

struct score *score_get(void);
void score_set_photo(int id);
void score_display(void);

#endif /* !SCORE_H */
