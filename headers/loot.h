#ifndef LOOT_H
# define LOOT_H

# include "elements.h"
# include "engine.h"

ENGINE_DEF_AGENT(loot, 0, 1, GLOOP_COLLISIONS, LOOT)
struct loot
{
    ENGINE_AGENT(loot);

    int pos;
};
ENGINE_DEF_MAKE(loot)

void loot_init(struct loot *loot);

void loot_collision(struct loot *loot, struct agent *collider);

void loot_update(struct loot *loot, double delta);

void loot_did_enter_scene(struct loot *loot);

void loot_draw(struct loot *loot);

void loot_destroy(struct loot *loot);

#endif /* !LOOT_H */
