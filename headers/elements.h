#ifndef ELEMENTS_H
# define ELEMENTS_H

#define NB_ELEMENTS 60

#define PLAYER_SPRITES  \
    PLAYER,             \
    PLAYER1,            \
    PLAYER2,            \
    PLAYER3,            \
    PLAYER4,            \
    PLAYER5,            \
    PLAYER6,            \
    PLAYER7,            \
    PLAYER8,            \
    PLAYER_RUN,         \
    PLAYER_RUN1,        \
    PLAYER_RUN2,        \
    PLAYER_RUN3,        \
    PLAYER_RUN4,        \
    PLAYER_RUN5,        \
    PLAYER_ATTACKING,   \
    PLAYER_ATTACKING1,   \
    PLAYER_ATTACKING2,   \
    PLAYER_ATTACKING3,   \
    PLAYER_ATTACKING4,   \
    PLAYER_ATTACKING5,   \
    PLAYER_ATTACK,      \
    PLAYER_ATTACK1,     \
    PLAYER_ATTACK2,     \
    PLAYER_ATTACK3,     \
    PLAYER_ATTACK4,     \
    PLAYER_ATTACK5,     \
    PLAYER_ATTACK6,     \
    PLAYER_ATTACK7,     \
    PLAYER_ATTACK8      \

enum elements
{
  NONE = 0,
  BLOCK,
  WATER,
  PLAYER_SPRITES,
  DEMON,
  FIREBALL,
  PLATFALL,
  SPIKES,
  LOOT,
  EXIT,
  RETRY,
  GOTO2,
  GOTO3,
  BOSS,
  PHOTO /* has to be the last */
};

#endif /* !ELEMENTS_H */
