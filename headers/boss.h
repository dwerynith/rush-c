#ifndef BOSS_H
#define BOSS_H

#include "elements.h"
#include "engine.h"

enum boss_state
{
  BOSS_SCREAM,
  BOSS_PREPARE_JUMP,
  BOSS_JUMP,
  BOSS_DEFLG,
  BOSS_FALL,
  BOSS_LAND,
  BOSS_SHOOT,
  BOSS_IDLE,
  BOSS_DEAD
};

#define NB_CLIPS_SCREAM 10
#define NB_CLIPS_IDLE 2
#define NB_CLIPS_SHOOT 1
#define NB_CLIPS_PREPARE_JUMP 1
#define NB_CLIPS_JUMP 1
#define NB_CLIPS_DEFLG 8
#define NB_CLIPS_FALL 1
#define NB_CLIPS_DEAD 4

#define TICK_SCREAM 200
#define TICK_IDLE 500
#define TICK_SHOOT 500
#define TICK_PREPARE_JUMP 350
#define TICK_JUMP 0
#define TICK_DEFLG 135
#define TICK_FALL 0
#define TICK_DEAD 250

#define HEALTH_MAX 10

ENGINE_DEF_AGENT(boss, 0, 1, GLOOP_UPDATE | GLOOP_COLLISIONS | GLOOP_RIGID,
                 BOSS)
struct boss
{
  ENGINE_AGENT(boss);

  enum boss_state state;
  double state_duration;
  int clip_idx;
  int clip_cnt;
  double tick;
  SDL_Rect *clips;
  double scale;

  SDL_Rect clips_scream[NB_CLIPS_SCREAM];
  SDL_Rect clips_idle[NB_CLIPS_IDLE];
  SDL_Rect clips_shoot[NB_CLIPS_SHOOT];
  SDL_Rect clips_prepare_jump[NB_CLIPS_PREPARE_JUMP];
  SDL_Rect clips_jump[NB_CLIPS_JUMP];
  SDL_Rect clips_deflg[NB_CLIPS_DEFLG];
  SDL_Rect clips_fall[NB_CLIPS_FALL];
  SDL_Rect clips_dead[NB_CLIPS_DEAD];

  double fall_dx;
  double fall_dy;
  int player_got_it;
  int i_got_it;

  int xspeed;
  int yspeed;
  int direction;
  double health;

  int show_acu;
  double acu_anim;
};
ENGINE_DEF_MAKE(boss)

void boss_init(struct boss *boss);

void boss_collision(struct boss *boss, struct agent *collider);

void boss_update(struct boss *boss, double delta);

void boss_did_enter_scene(struct boss *boss);

void boss_draw(struct boss *boss);

void boss_destroy(struct boss *boss);

#endif /* !BOSS_H */
