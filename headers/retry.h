#ifndef RETRY_H
# define RETRY_H

# include "elements.h"
# include "engine.h"

ENGINE_DEF_AGENT(retry, 0, 1, GLOOP_COLLISIONS, RETRY)
struct retry
{
    ENGINE_AGENT(retry);

    int pos;
};
ENGINE_DEF_MAKE(retry)

void retry_init(struct retry *retry);

void retry_collision(struct retry *retry, struct agent *collider);

void retry_update(struct retry *retry, double delta);

void retry_did_enter_scene(struct retry *retry);

void retry_draw(struct retry *retry);

void retry_destroy(struct retry *retry);

#endif /* !RETRY_H */
