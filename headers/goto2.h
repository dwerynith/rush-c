#ifndef GOTO2_H
# define GOTO2_H

# include "elements.h"
# include "engine.h"

ENGINE_DEF_AGENT(goto2, 0, 1, GLOOP_COLLISIONS, GOTO2)
struct goto2
{
    ENGINE_AGENT(goto2);

    int pos;
};
ENGINE_DEF_MAKE(goto2)

void goto2_init(struct goto2 *goto2);

void goto2_collision(struct goto2 *goto2, struct agent *collider);

void goto2_update(struct goto2 *goto2, double delta);

void goto2_did_enter_scene(struct goto2 *goto2);

void goto2_draw(struct goto2 *goto2);

void goto2_destroy(struct goto2 *goto2);

#endif /* !GOTO2_H */
