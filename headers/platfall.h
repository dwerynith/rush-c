#ifndef PLATFALL_H
# define PLATFALL_H

# include "elements.h"
# include "engine.h"

ENGINE_DEF_AGENT(platfall, 0, 1, GLOOP_UPDATE | GLOOP_RIGID, PLATFALL)
struct platfall
{
    ENGINE_AGENT(platfall);

    int yspeed;

    float gravity;
};
ENGINE_DEF_MAKE(platfall)

void platfall_init(struct platfall *platfall);

void platfall_collision(struct platfall *platfall, struct agent *collider);

void platfall_update(struct platfall *platfall, double delta);

void platfall_did_enter_scene(struct platfall *platfall);

void platfall_draw(struct platfall *platfall);

void platfall_destroy(struct platfall *platfall);

#endif /* !PLATFALL_H */
