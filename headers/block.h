#ifndef BLOCK_H_
# define BLOCK_H_

# include "elements.h"
# include "engine.h"

ENGINE_DEF_AGENT(block, 0, 1, GLOOP_RIGID, BLOCK)
struct block
{
    ENGINE_AGENT(block);
};
ENGINE_DEF_MAKE(block)

void block_init(struct block *block);

void block_collision(struct block *block, struct agent *collider);

void block_update(struct block *block, double delta);

void block_did_enter_scene(struct block *block);

void block_draw(struct block *block);

void block_destroy(struct block *block);

#endif /* !BLOCK_H_ */
