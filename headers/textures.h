#ifndef TEXTURES_H
# define TEXTURES_H

# include <SDL2/SDL.h>

# include "elements.h"
# include "agent.h"

SDL_Texture *textures_get(enum elements elt);

void textures_init_all(void);

void textures_destroy_all(void);

#endif /* !TEXTURES_H */
