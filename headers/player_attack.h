#ifndef PLAYER_ATTACK_H_
# define PLAYER_ATTACK_H_

# include "elements.h"
# include "engine.h"
# include "player.h"

ENGINE_DEF_AGENT(player_attack, 1, 1,
                 GLOOP_COLLISIONS | GLOOP_UPDATE | GLOOP_RIGID, PLAYER_ATTACK)
struct player_attack
{
    ENGINE_AGENT(player_attack);
    int image;
    int direction;

    struct player *player;
};
ENGINE_DEF_MAKE(player_attack)

void player_attack_init(struct player_attack *player_attack);

void player_attack_collision(struct player_attack *player_attack,
                             struct agent *collider);

void player_attack_update(struct player_attack *player_attack, double delta);

void player_attack_did_enter_scene(struct player_attack *player_attack);

void player_attack_draw(struct player_attack *player_attack);

void player_attack_destroy(struct player_attack *player_attack);

#endif /* !PLAYER_ATTACK_H_ */
