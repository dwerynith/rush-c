#ifndef WATER_H_
# define WATER_H_

# include "elements.h"
# include "engine.h"

ENGINE_DEF_AGENT(water, 0, 1, GLOOP_RIGID, WATER)
struct water
{
    ENGINE_AGENT(water);
};
ENGINE_DEF_MAKE(water)

void water_init(struct water *water);

void water_collision(struct water *water, struct agent *collider);

void water_update(struct water *water, double delta);

void water_did_enter_scene(struct water *water);

void water_draw(struct water *water);

void water_destroy(struct water *water);

#endif /* !WATER_H_ */
