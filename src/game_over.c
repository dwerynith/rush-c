#include "scenes.h"
#include "score.h"
#include "maps.h"

void game_over_scene_init(struct scene *scene)
{
  scene->view.w = ENGINE_WIN_WIDTH;
  scene->view.h = ENGINE_WIN_HEIGHT;
  map_load("../maps/game_over.map");
}

void game_over_scene_destroy(struct scene *scene)
{
  free(scene);
}

struct scene *game_over_scene_make(void)
{
    struct scene *s = engine_scene_make();

    score_display();
    s->init = game_over_scene_init;
    s->destroy = game_over_scene_destroy;

    return s;
}
