#include <stddef.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>

#include "goto2.h"
#include "goto3.h"
#include "block.h"
#include "demon.h"
#include "exit.h"
#include "loot.h"
#include "maps.h"
#include "platfall.h"
#include "player.h"
#include "player_attack.h"
#include "retry.h"
#include "spikes.h"
#include "score.h"
#include "water.h"
#include "boss.h"

static void element_load(char c, size_t x, size_t y)
{
  void *init = NULL;
  switch (c)
  {
    case 'b':
      init = block_make();
      break;
    case 'd':
      init = demon_make();
      break;
    case 'w':
      init = water_make();
      break;
    case 'p':
      init = platfall_make();
      break;
    case 's':
      init = spikes_make();
      break;
    case 'o':
      init = loot_make();
      break;
    case 'e':
      init = exit_make();
      break;
    case 'r':
      init = retry_make();
      break;
    case '2':
      init = goto2_make();
      break;
    case '3':
      init = goto3_make();
      break;
    case 'B':
      init = boss_make();
      break;
    default:
      break;
  }
  if (!init)
    return;
  struct agent *agent = init;
  agent->rect.x = x;
  agent->rect.y = y;

  engine_agent_add(agent);
}

void map_load(char *path)
{
  FILE *map = fopen(path, "r");
  if (!map)
    return;

  struct engine *engine = engine_get();
  char background[300] =
  {
    0
  };
  engine->scene->dim.x = 0;
  engine->scene->dim.y = 0;

  fscanf(map, "width %d\n", &engine->scene->dim.w);
  fscanf(map, "height %d\n", &engine->scene->dim.h);
  fscanf(map, "background %s\n", background);

  SDL_Surface *img = IMG_Load(background);
  SDL_Texture *texture = SDL_CreateTextureFromSurface(engine->renderer, img);
  SDL_FreeSurface(img);

  engine->scene->bg_tex = texture;


  size_t px = 0;
  size_t py = 0;
  if (EOF != fscanf(map, "player %zu,%zu\n", &px, &py))
  {
    struct player *pl = player_make();
    pl->rect.x = px;
    pl->rect.y = py;
    engine_agent_add((struct agent *)pl);
  }
  char line[2048] =
  {
    0
  };

  for (int i = 0; i < engine->scene->dim.h; i++)
  {
    if (!fgets(line, 2048, map))
      break;
    for (int j = 0; j < engine->scene->dim.w; j++)
    {
      element_load(line[j], j, i);
    }
  }

  engine->scene->dim.w *= 32;
  engine->scene->dim.h *= 32;

  fclose(map);
}
