#include <stdio.h>
#include <stdlib.h>

#include "elements.h"
#include "engine.h"
#include "textures.h"
#include "score.h"

#include "block.h"
#include "maps.h"
#include "player.h"
#include "water.h"

static void first_scene_init(struct scene *scene)
{
  scene->view.w = ENGINE_WIN_WIDTH;
  scene->view.h = ENGINE_WIN_HEIGHT;
  map_load("../maps/long.map");
}
static void first_scene_destroy(struct scene *scene)
{
  free(scene);
}

struct scene *first_scene(void)
{
  struct scene *s = engine_scene_make();
  s->init = first_scene_init;
  s->destroy = first_scene_destroy;
  return s;
}

static struct agent *scene_clear(struct scene *scene)
{
  if (!scene)
    return NULL;

  struct agent *kept = NULL;

  struct agent *it = scene->first;
  while (it)
  {
    struct agent *next = it->next;
    if (it->meta->survives_scene)
    {
      if (kept)
        it->next = kept;
      kept = it;
      kept->prev = NULL;
    }
    else
      it->meta->destroy(it);
    it = next;
  }

  scene->destroy(scene);
  return kept;
}

static void check_collisions(struct engine *engine)
{
  struct agent *it = engine->scene->first;
  while (it)
  {
    struct agent *inext = it->next;
    if (it->meta->gloop_flags & GLOOP_RIGID)
    {
      struct agent *jt = engine->scene->first;
      while(jt)
      {
        struct agent *jnext = jt->next;
        if (it != jt && (jt->meta->gloop_flags & GLOOP_COLLISIONS)
            && SDL_HasIntersection(&it->rect, &jt->rect) == SDL_TRUE)
          jt->meta->collision(jt, it);
        jt = jnext;
      }
    }
    it = inext;
  }
}

static void draw_scene(struct engine *engine)
{
  SDL_RenderClear(engine->renderer);

  engine->scene->dim.x = -engine->scene->view.x;
  engine->scene->dim.y = -engine->scene->view.y;
  SDL_RenderCopy(engine->renderer, engine->scene->bg_tex,
      NULL, &engine->scene->dim);
  for (struct agent *it = engine->scene->first; it; it = it->next)
    it->meta->draw(it);
  SDL_RenderPresent(engine->renderer);
}

int main(void)
{
  struct engine *engine = engine_get();
  textures_init_all();
  engine->next_scene = first_scene();

  uint64_t time_now = 0;
  uint64_t time_last = 0;
  double frequency = SDL_GetPerformanceFrequency();

  while (engine->running)
  {
    SDL_Event event;
    if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
      break;

    if (engine->next_scene)
    {
      // clear scene & keep some nodes
      struct agent *survivors = scene_clear(engine->scene);

      // repl scene
      engine->scene = engine->next_scene;
      engine->next_scene = NULL;

      // add kept nodes
      for (struct agent *it = survivors; it; it = it->next)
        engine_agent_add(it);

      // intialize the scene
      engine->scene->init(engine->scene);

      // initialize new nodes
      for (struct agent *it = engine->scene->first; it; it = it->next)
        it->meta->init(it);
    }

    check_collisions(engine);

    static const double fps_rate = 100.0 / 5.0;
    time_last = time_now;
    time_now = SDL_GetPerformanceCounter();
    double delta_time = (time_now - time_last) * 1000.0 / frequency;
    Uint32 sleep = fps_rate - delta_time;
    if (sleep < fps_rate)
      SDL_Delay(sleep);

    struct agent *it = engine->scene->first;
    while (it)
    {
      struct agent *next = it->next;
      if (it->meta->gloop_flags & GLOOP_UPDATE)
        it->meta->update(it, delta_time);
      it = next;
    }

    draw_scene(engine);

  }

  for (struct agent *it = engine->scene->first; it;)
  {
    struct agent *next = it->next;
    it->meta->destroy(it->self);
    it = next;
  }
  engine->scene->destroy(engine->scene);

  textures_destroy_all();
  SDL_DestroyRenderer(engine->renderer);
  SDL_DestroyWindow(engine->window);

  return 0;
}
