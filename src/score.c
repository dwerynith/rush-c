#include "agent.h"
#include "engine.h"
#include "score.h"
#include "photos.h"

struct score *score_get(void)
{
  static struct score score =
  {
    .photos =
    {
      0
    }
  };

  return &score;
}

void score_set_photo(int id)
{
  struct score *score = score_get();
  score->photos[id] = 1;
}

static void score_draw_photo(int id)
{
  struct photo *photo = photo_make();
  photo->rect.x = (id % 6) * P_WIDTH + 64;
  photo->rect.y = (id / 6) * P_HEIGHT + 64;
  photo->rect.w = P_WIDTH;
  photo->rect.h = P_HEIGHT;
  photo->timeleft = -1;
  photo->id = id;

  engine_agent_add(agent_get(photo));
}

void score_display(void)
{
  struct score *score = score_get();
  for (int i = 0; i < NB_PHOTO; i++)
    if (score->photos[i])
      score_draw_photo(i);
  return;
}
