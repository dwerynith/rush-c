#include "engine.h"

struct engine *engine_get(void)
{
  static struct engine engine =
    {
      .running = 1,
      .scene = NULL,
      .next_scene = NULL,
      .window = NULL,
      .renderer = NULL
    };

  if (!engine.window)
  {
    engine.window = SDL_CreateWindow("A.C:U", SDL_WINDOWPOS_UNDEFINED,
                                     SDL_WINDOWPOS_UNDEFINED, ENGINE_WIN_WIDTH,
                                     ENGINE_WIN_HEIGHT, SDL_WINDOW_OPENGL
                                     | SDL_WINDOW_INPUT_GRABBED);
    engine.renderer = SDL_CreateRenderer(engine.window, -1,
                                         SDL_RENDERER_ACCELERATED);
  }

  return &engine;
}

void engine_agent_add(struct agent *agent)
{
  struct engine *e = engine_get();

  agent->next = NULL;
  agent->prev = NULL;

  if (e->scene->first)
  {
    agent->next = e->scene->first;
    agent->next->prev = agent;
  }
  e->scene->first = agent;
}

void engine_agent_remove(struct agent *agent)
{
  if (!agent)
    return;

  if (agent->next)
    agent->next->prev = agent->prev;
  if (agent->prev)
    agent->prev->next = agent->next;
  else
    engine_get()->scene->first = agent->next;

  agent->meta->destroy(agent);
}

struct scene *engine_scene_make(void)
{
  struct scene *s = malloc(sizeof(*s));
  s->view.x = 0;
  s->view.y = 0;
  s->view.w = 0;
  s->view.h = 0;
  s->first = NULL;
  return s;
}

long engine_agent_lookup_idx(struct agent *agent)
{
  struct engine *engine = engine_get();
  long idx = (engine->scene->view.w * (agent->rect.y - engine->scene->view.y)
              + (agent->rect.x - engine->scene->view.x));
  if (idx < 0 || idx >= ENGINE_LOOKUP_SIZE)
    return -1;
  return idx;
}
