#include "scenes.h"
#include "maps.h"

void level2_scene_init(struct scene *scene)
{
  scene->view.w = ENGINE_WIN_WIDTH;
  scene->view.h = ENGINE_WIN_HEIGHT;
  map_load("../maps/auto_scroll.map");
}

void level2_scene_destroy(struct scene *scene)
{
  free(scene);
}

struct scene *level2_scene_make(void)
{
    struct scene *s = engine_scene_make();
    s->init = level2_scene_init;
    s->destroy = level2_scene_destroy;

    return s;
}
