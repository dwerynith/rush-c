#include <stdlib.h>

#include "textures.h"
#include "water.h"

void water_init(struct water *water)
{
    water->rect.x *= 32;
    water->rect.y *= 32;
    water->rect.w = 32;
    water->rect.h = 32;
}

void water_collision(struct water *water, struct agent *collider)
{
    water = water;
    collider = collider;
}

void water_update(struct water *water, double delta)
{
    water = water;
    delta = delta;
}

void water_did_enter_scene(struct water *water)
{
    water = water;
}

void water_draw(struct water *water)
{
  struct engine *engine = engine_get();
  SDL_Rect pos =
  {
    .x = water->rect.x - engine->scene->view.x,
    .y = water->rect.y - engine->scene->view.y,
    .w = water->rect.w,
    .h = water->rect.h
  };
   SDL_RenderCopy(engine_get()->renderer, textures_get(water->meta->elt),
                   NULL, &pos);
}

void water_destroy(struct water *water)
{
    free(water);
}
