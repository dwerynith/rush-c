#include <stdlib.h>

#include "loot.h"
#include "photos.h"
#include "score.h"
#include "textures.h"

void loot_init(struct loot *loot)
{
  loot->rect.x *= 32;
  loot->rect.y *= 32;
  loot->rect.w = 32;
  loot->rect.h = 32;
  loot->pos = 0;
}

void loot_collision(struct loot *loot, struct agent *collider)
{
  loot = loot;
  if (collider->meta->elt == PLAYER)
  {
    struct photo *photo = photo_make();
    photo->rect.x = loot->rect.x;
    photo->rect.y = loot->rect.y;
    photo->meta->init(photo);

    score_set_photo(photo->id);

    engine_agent_add(agent_get(photo));
    engine_agent_remove(agent_get(loot));
  }
}

void loot_update(struct loot *loot, double delta)
{
  loot = loot;
  delta = delta;
}

void loot_did_enter_scene(struct loot *loot)
{
  loot = loot;
}

void loot_draw(struct loot *loot)
{
  struct engine *engine = engine_get();
  SDL_Rect pos =
  {
    .x = loot->rect.x - engine->scene->view.x,
    .y = loot->rect.y - engine->scene->view.y,
    .w = loot->rect.w,
    .h = loot->rect.h
  };
  SDL_RenderCopy(engine_get()->renderer, textures_get(loot->meta->elt),
                 NULL, &pos);
}

void loot_destroy(struct loot *loot)
{
  free(loot);
}
