#include <stdlib.h>

#include "engine.h"
#include "goto2.h"
#include "photos.h"
#include "scenes.h"
#include "textures.h"

void goto2_init(struct goto2 *goto2)
{
  goto2->rect.x *= 32;
  goto2->rect.y *= 32;
  goto2->rect.w = 32;
  goto2->rect.h = 160;
  goto2->pos = 0;
}

void goto2_collision(struct goto2 *goto2, struct agent *collider)
{
  goto2 = goto2;
  if (collider->meta->elt == PLAYER)
  {
    struct scene *level2_scene = level2_scene_make();
    engine_agent_remove(collider);
    engine_get()->next_scene = level2_scene;
  }
}

void goto2_update(struct goto2 *goto2, double delta)
{
  goto2 = goto2;
  delta = delta;
}

void goto2_did_enter_scene(struct goto2 *goto2)
{
  goto2 = goto2;
}

void goto2_draw(struct goto2 *goto2)
{
  SDL_RendererFlip flip = SDL_FLIP_HORIZONTAL;
  SDL_RenderCopyEx(engine_get()->renderer, textures_get(goto2->meta->elt),
                 NULL, &goto2->rect, 0, NULL, flip);
}

void goto2_destroy(struct goto2 *goto2)
{
  free(goto2);
}
