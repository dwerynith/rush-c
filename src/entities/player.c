#include <stdlib.h>
#include <string.h>

#include "fireball.h"
#include "platfall.h"
#include "player.h"
#include "player.h"
#include "player_attack.h"
#include "scenes.h"
#include "textures.h"
#include "health_bar.h"

#define Y_ACCEL -10

#define X_ACCEL 3
#define XMAX_SPEED 7
#define X_DECAY 1

#define CD 50
#define AIR_CD 20
#define INVI_FRAME 50

void player_init(struct player *player)
{
    player->rect.x *= 32;
    player->rect.y *= 32;
    player->rect.w = 64;
    player->rect.h = 64;

    player->direction = 1;

    player->xspeed = 0;
    player->yspeed = 0;
    player->gravity = .5f;

    player->cooldown = 0;
    player->air_cd = 0;

    player->health = 4;
    player->invincibility = 0;

    player->img_start = 0;
    player->frame = 0;
    player->attacking = 0;

    player->isgod = 0;
    player->gmcd = 0;
    player->can_move = 1;
}

static void update_variables(struct player *player)
{
    if (!player->can_move)
        return;
    player->rect.x += player->xspeed;
    if (!player->on_ground)
    {
        player->rect.y += player->yspeed;
    }
    else
    {
        player->yspeed = 0;
        player->on_ground = 0;
    }
    player->yspeed += player->gravity;

    if (player->xspeed < -1)
        player->xspeed += X_DECAY;
    else if (player->xspeed > 1)
        player->xspeed -= X_DECAY;
    else
        player->xspeed = 0;
    if (player->xspeed > 0)
        player->direction = 1;
    if (player->xspeed < 0)
        player->direction = 0;

    --player->cooldown;
    if (player->cooldown < 0)
        player->cooldown = 0;

    --player->air_cd;
    if (player->air_cd < 0)
        player->air_cd = 0;

    --player->gmcd;
    if (player->gmcd < 0)
        player->gmcd = 0;

    --player->invincibility;
    if (player->invincibility < 0)
        player->invincibility = 0;
    if (player->isgod && player->invincibility < 1)
        player->invincibility = INVI_FRAME;
}

static void game_over(struct player *player)
{
    if (player->on_ground && player->health > -10)
    {
        player->health = -10;
        player->yspeed = Y_ACCEL;
        player->on_ground = 0;
    }
    update_variables(player);
    if (player->health-- > -90)
        return;

    engine_agent_remove(agent_get(player));
    struct scene *game_over_scene = game_over_scene_make();

    engine_get()->next_scene = game_over_scene;
}

static void update_camera(SDL_Rect pos)
{
    struct scene *scene = engine_get()->scene;

    scene->view.x = pos.x - scene->view.w / 2;
    scene->view.y = pos.y - scene->view.h / 2;

    if (scene->view.x < 0)
        scene->view.x = 0;
    else if (scene->view.x > scene->dim.w - scene->view.w)
        scene->view.x = scene->dim.w - scene->view.w;

    if (scene->view.y < 0)
        scene->view.y = 0;
    else if (scene->view.y > scene->dim.h - scene->view.h)
        scene->view.y = scene->dim.h - scene->view.h;

}

static void no_more_projectiles(void)
{
    struct agent *agent = engine_get()->scene->first;
    struct agent *next = agent;
    while (next)
    {
        next = agent->next;
        if (agent->meta->elt == FIREBALL)
            engine_agent_remove(agent);
        agent = next;
    }
}

void player_update(struct player *player, double delta)
{

    if (player->health < 1)
    {
        game_over(player);
        return;
    }
    delta = delta;
    const Uint8 *state = SDL_GetKeyboardState(NULL);

    if (state[SDL_SCANCODE_D] || state[SDL_SCANCODE_RIGHT])
    {
        player->xspeed += X_ACCEL;
        if (player->xspeed > XMAX_SPEED)
            player->xspeed = XMAX_SPEED;
    }
    if (state[SDL_SCANCODE_A] || state[SDL_SCANCODE_LEFT])
    {
        player->xspeed -= X_ACCEL;
        if (player->xspeed < -XMAX_SPEED)
            player->xspeed = -XMAX_SPEED;
    }
    if (state[SDL_SCANCODE_G] && !player->gmcd)
    {
        player->isgod = 1 - player->isgod;
        player->gmcd = AIR_CD;
    }

    if (state[SDL_SCANCODE_SPACE]
        && (player->cooldown <= 0 || player->isgod))
    {
        player->attacking = 1;
        player->cooldown = CD;
    }

    if (state[SDL_SCANCODE_DELETE])
        no_more_projectiles();

    if (player->attacking && (player->frame == 8 || player->isgod))
    {
        struct player_attack *atk= player_attack_make();
        atk->direction = player->direction;
        if (!player->direction)
            atk->rect.x = player->rect.x - 64;
        else
            atk->rect.x = player->rect.x + 64;
        atk->image = 0;
        atk->rect.y = player->rect.y;
        atk->player = player;
        engine_agent_add(agent_get(atk));
        player_attack_init(atk);
    }
    if (state[SDL_SCANCODE_W] || state[SDL_SCANCODE_UP])
    {
        if (player->on_ground || player->isgod)
        {
            player->yspeed = Y_ACCEL;
            player->on_ground = 0;
        }
        else if (player->double_jump && !player->air_cd)
        {
            player->yspeed = Y_ACCEL;
            player->double_jump = 0;
            player->air_cd = AIR_CD;
        }
    }
    update_variables(player);
    update_camera(player->rect);
}

void player_collision(struct player *player, struct agent *collider)
{
    int px = player->rect.x + 32;
    int py = player->rect.y + 32;

    int bx = collider->rect.x + 16;
    int by = collider->rect.y + 16;

    if (collider->meta->elt == BLOCK)
    {
        if (py < by && px < bx + 32 && px > bx - 32)
        {
            player->air_cd = AIR_CD;
            player->double_jump = 1;
            player->on_ground = 1;
            player->rect.y = collider->rect.y - 63;
        }
        else if (py > by && px < bx + 32 && px > bx - 32)
        {
            player->yspeed = 0;
            player->rect.y = collider->rect.y + 33;
        }
        else if (px < bx && py < by + 32 && py > by - 32)
            player->rect.x = collider->rect.x - 65;

        else if (px > bx && py < by + 32 && py > by - 32)
            player->rect.x = collider->rect.x + 33;

        if (player->rect.y < collider->rect.y + 1
                && player->rect.y > collider->rect.y -1)
        {
            if (player->rect.x < collider->rect.x + 64)
                player->rect.x = collider->rect.x - 64;

            if (player->rect.x > collider->rect.x - 64)
                player->rect.x = collider->rect.x + 64;
        }
    }
    else if (collider->meta->elt == PLATFALL)
    {
        if (py < by && px < bx + 80 && px > bx - 16)
        {
            void *tmp = collider;
            struct platfall *pf = tmp;
            platfall_collision(pf, agent_get(player));
        }
        else if (py > by && px < bx + 80 && px > bx - 16)
            player->rect.y = collider->rect.y + 33;

    }
    else if (collider->meta->elt == FIREBALL)
    {
        if (!player->invincibility
                && !(player->attacking && player->frame < 25))
        {
            --player->health;
            player->invincibility = INVI_FRAME;
        }
        void *tmp = collider;
        struct fireball *fb = tmp;
        fireball_collision(fb, agent_get(player));
    }
    else if ((collider->meta->elt == DEMON || collider->meta->elt == SPIKES
                || collider->meta->elt == WATER)
            && !player->invincibility
            && !(player->attacking && player->frame < 25))
    {
        --player->health;
        player->invincibility = INVI_FRAME;
    }
}

void player_did_enter_scene(struct player *player)
{
    player = player;
}

static int player_sprite(struct player *player)
{
    ++player->frame;
    if (player->attacking)
    {
        if (player->attacking == 1)
        {
            player->attacking = 2;
            player->frame = 0;
        }
        player->img_start = 15;
        if (player->frame > 47)
            player->attacking = 0;
        return player->img_start + player->frame / 8;
    }
    else if (!player->xspeed)
    {
        player->img_start = 0;
        if (player->frame > 67)
            player->frame = 0;
    }
    else
    {
        player->img_start = 9;
        if (player->frame > 33)
            player->frame = 0;
    }
    return player->img_start + player->frame / 8;
}


void player_draw(struct player *player)
{
    struct engine *engine = engine_get();
    SDL_Rect pos =
    {
        .x = player->rect.x - engine->scene->view.x,
        .y = player->rect.y - engine->scene->view.y,
        .w = player->rect.w,
        .h = player->rect.h
    };

    SDL_RendererFlip flip = SDL_FLIP_HORIZONTAL & !player->direction;
    double angle = player->health > 0 ? 0 : 90;
    angle *= player->direction ? -1 : 1;
    if (player->invincibility % 3 == 0)
        SDL_RenderCopyEx(engine_get()->renderer,
                textures_get(player->meta->elt + player_sprite(player)),
                NULL, &pos, angle, NULL, flip);

    pos.h = 5;
    pos.y -= 10;
    pos.x -= 0.15 * pos.w;
    pos.w *= 1.25;
    draw_health_bar(pos, (1.0 * player->health > 0 ? player->health : 0) / 4.0);
}

void player_destroy(struct player *player)
{
    free(player);
}
