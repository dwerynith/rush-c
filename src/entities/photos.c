#include <stdlib.h>
#include <time.h>

#include "photos.h"
#include "textures.h"

void photo_init(struct photo *photo)
{
  static int id = -1;
  if (id < 0)
  {
    srand(time(NULL));
    id = rand() % NB_PHOTO;
  }


  photo->rect.x -= 16;
  photo->rect.y -= 96;
  photo->rect.w = 90;
  photo->rect.h = 128;

  photo->id = id++ % NB_PHOTO;

  photo->timeleft = PHOTO_TIME;
}

void photo_collision(struct photo *photo, struct agent *collider)
{
    photo = photo;
    collider = collider;
}

void photo_update(struct photo *photo, double delta)
{
    photo->timeleft -= photo->timeleft > 0 ? 1 : 0;
    delta = delta;

    if (photo->timeleft == 0)
      engine_agent_remove(agent_get(photo));
}

void photo_did_enter_scene(struct photo *photo)
{
    photo = photo;
}

void photo_draw(struct photo *photo)
{
  struct engine *engine = engine_get();
  SDL_Rect pos =
  {
    .x = photo->rect.x - engine->scene->view.x,
    .y = photo->rect.y - engine->scene->view.y,
    .w = photo->rect.w,
    .h = photo->rect.h
  };
  SDL_RenderCopy(engine_get()->renderer, textures_get(PHOTO + photo->id),
                   NULL, &pos);
}

void photo_destroy(struct photo *photo)
{
    free(photo);
}
