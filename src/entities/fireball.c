#include <stdlib.h>
#include <string.h>

#include "fireball.h"
#include "textures.h"

#define Y_ACCEL -10

#define X_ACCEL 3
#define XMAX_SPEED 7
#define X_DECAY 1


void fireball_init(struct fireball *fireball)
{
    fireball->rect.w = 32;
    fireball->rect.h = 32;
}

void fireball_update(struct fireball *fireball, double delta)
{
  if (!fireball->xspeed && !fireball->yspeed)
  {
    engine_agent_remove(agent_get(fireball));
    return;
  }
  delta = delta;
  fireball->rect.x += fireball->xspeed;
  fireball->rect.y += fireball->yspeed;
}

void fireball_collision(struct fireball *fireball, struct agent *collider)
{
    if (collider->meta->elt != DEMON
        && collider->meta->elt != FIREBALL
        && collider->meta->elt != BOSS)
        engine_agent_remove((struct agent *)fireball);
}

void fireball_did_enter_scene(struct fireball *fireball)
{
    fireball = fireball;
}

void fireball_draw(struct fireball *fireball)
{
  struct engine *engine = engine_get();
  SDL_Rect pos =
  {
    .x = fireball->rect.x - engine->scene->view.x,
    .y = fireball->rect.y - engine->scene->view.y,
    .w = fireball->rect.w,
    .h = fireball->rect.h
  };
  SDL_RenderCopy(engine_get()->renderer, textures_get(fireball->meta->elt),
            NULL, &pos);
}

void fireball_destroy(struct fireball *fireball)
{
    free(fireball);
}
