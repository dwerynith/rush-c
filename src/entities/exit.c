#include <stdlib.h>

#include "exit.h"
#include "photos.h"
#include "textures.h"

void exit_init(struct exit *exit)
{
  exit->rect.x *= 32;
  exit->rect.y *= 32;
  exit->rect.w = 32;
  exit->rect.h = 160;
  exit->pos = 0;
}

void exit_collision(struct exit *exit, struct agent *collider)
{
  exit = exit;
  if (collider->meta->elt == PLAYER)
  {
    engine_get()->running = 0;
  }
}

void exit_update(struct exit *exit, double delta)
{
  exit = exit;
  delta = delta;
}

void exit_did_enter_scene(struct exit *exit)
{
  exit = exit;
}

void exit_draw(struct exit *exit)
{
  SDL_RenderCopy(engine_get()->renderer, textures_get(exit->meta->elt),
                 NULL, &exit->rect);
}

void exit_destroy(struct exit *exit)
{
  free(exit);
}
