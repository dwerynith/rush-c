#include <stdlib.h>

#include "spikes.h"
#include "player.h"
#include "textures.h"

void spikes_init(struct spikes *spikes)
{
  spikes->rect.x = (spikes->rect.x - 4) * 32;
  spikes->rect.y *= 32;
  spikes->rect.w = 32;
  spikes->rect.h = 32;

  spikes->xspeed = 5;
}

void spikes_collision(struct spikes *spikes, struct agent *collider)
{
    spikes = spikes;
    collider = collider;
    if (collider->meta->elt == PLAYER)
    {
        void *tmp = collider;
        struct player *player = tmp;
        if (!player->isgod)
            player->health = 0;
    }
}

void spikes_update(struct spikes *spikes, double delta)
{
  delta = delta;
  spikes->rect.x += spikes->xspeed;
}

void spikes_did_enter_scene(struct spikes *spikes)
{
  spikes = spikes;
}

void spikes_draw(struct spikes *spikes)
{
  struct engine *engine = engine_get();
  SDL_Rect pos =
  {
    .x = spikes->rect.x - engine->scene->view.x,
    .y = spikes->rect.y - engine->scene->view.y,
    .w = spikes->rect.w,
    .h = spikes->rect.h
  };
  SDL_RenderCopy(engine_get()->renderer, textures_get(spikes->meta->elt),
                 NULL, &pos);
}

void spikes_destroy(struct spikes *spikes)
{
  free(spikes);
}
