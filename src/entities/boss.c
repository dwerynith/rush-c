#include <stdio.h>
#include <math.h>
#include <SDL2/SDL.h>

#include "player.h"
#include "fireball.h"
#include "health_bar.h"
#include "textures.h"
#include "score.h"
#include "photos.h"
#include "boss.h"
#include "scenes.h"

#define SWITCH_STATE(Name, Dc)                          \
  boss->state = BOSS_ ## Name;                          \
  boss->clips = boss->clips_ ## Dc;                     \
  boss->clip_idx = 0;                                   \
  boss->tick = TICK_ ## Name;                           \
  boss->i_got_it = 0;                                   \
  boss->clip_cnt = NB_CLIPS_ ## Name

static struct player *player_get(void)
{
  struct engine *engine = engine_get();
  for (struct agent *it = engine->scene->first; it; it = it->next)
    if (it->meta->elt == PLAYER)
      return (struct player *)it;
  return NULL;
}

static int boss_rel_player_pos(struct boss *boss)
{
  return boss->rect.x < player_get()->rect.x ? 1 : -1;
}

static SDL_Rect get_acu_pos(int i, struct boss *boss)
{
  struct player *pl = player_get();
  if (boss->acu_anim <= 4000)
  {
    SDL_Rect pos =
      {
        .x = (engine_get()->scene->view.w - 5 * P_WIDTH) / 2 + (i % 5)
             * P_WIDTH - pl->rect.x,
        .y = 32 + (i / 5) * P_HEIGHT - pl->rect.y,
        .w = P_WIDTH,
        .h = P_HEIGHT
      };
    double r = (boss->acu_anim > 2000 ? 2000 : boss->acu_anim) / 2000;
    pos.x *= r;
    pos.y *= r;
    pos.x += pl->rect.x;
    pos.y += pl->rect.y;
    return pos;
  }
  else
  {
    if (boss->acu_anim >= 6000)
      boss->acu_anim = 6000;
    double p = boss->acu_anim;
    boss->acu_anim = 2000;
    SDL_Rect pos = boss->rect;
    SDL_Rect acu = get_acu_pos(i, boss);
    boss->acu_anim = p;

    double r = (boss->acu_anim - 4000) / 2000;
    pos.x -= acu.x;
    pos.y -= acu.y;
    pos.x *= r;
    pos.y *= r;
    pos.x += acu.x;
    pos.y += acu.y;

    pos.w = acu.w;
    pos.h = acu.h;

    return pos;
  }
}


static SDL_Rect rect_set(int x, int y, int w, int h)
{
  SDL_Rect rect =
  {
    .x = x,
    .y = y,
    .w = w,
    .h = h
  };

  return rect;
}

void boss_init(struct boss *boss)
{
  boss->rect.x *= 32;
  boss->rect.y *= 32;
  boss->rect.w = 128;
  boss->rect.h = 128;

  boss->clips_idle[0] = rect_set( 107, 703, 158, 204 );
  boss->clips_idle[1] = rect_set( 496, 699, 168, 206 );

  boss->clips_scream[0] = rect_set( 20,109,134,171 );
  boss->clips_scream[1] = rect_set( 20,109,134,171 );
  boss->clips_scream[2] = rect_set( 20,109,134,171 );
  boss->clips_scream[3] = rect_set( 20,109,134,171 );
  boss->clips_scream[4] = rect_set( 20,109,134,171 );
  boss->clips_scream[5] = rect_set( 175,105,135,175 );
  boss->clips_scream[6] = rect_set( 333,26,191,256 );
  boss->clips_scream[7] = rect_set( 535,37,320,245 );
  boss->clips_scream[8] = rect_set( 852,66,276,211 );
  boss->clips_scream[9] = rect_set( 1153,68,184,216 );

  boss->clips_shoot[0] = rect_set( 607,1471,184,206 );

  boss->clips_prepare_jump[0] = rect_set( 713,712,202,187 );
  boss->clips_jump[0] = rect_set(981,664,205,252 );

  boss->clips_deflg[0] = rect_set( 91,967,184,206 );
  boss->clips_deflg[1] = rect_set( 571,963,185,206 );
  boss->clips_deflg[2] = rect_set( 333,968,201,206 );
  boss->clips_deflg[3] = rect_set( 998,964,180,206 );
  boss->clips_deflg[4] = rect_set( 998,964,180,206 );
  boss->clips_deflg[5] = rect_set( 998,964,180,206 );
  boss->clips_deflg[6] = rect_set( 784,966,180,207 );
  boss->clips_deflg[7] = rect_set( 91,967,184,206 );

  boss->clips_fall[0] = rect_set( 352, 1719, 116, 165 );

  boss->clips_dead[0] = boss->clips_scream[9];
  boss->clips_dead[1] = boss->clips_scream[8];
  boss->clips_dead[2] = boss->clips_scream[7];
  boss->clips_dead[3] = boss->clips_scream[8];

  SWITCH_STATE(SCREAM, scream);
  boss->direction = 0;

  boss->health = HEALTH_MAX;

  boss->scale = 2.0;

  boss->player_got_it = 0;
  boss->fall_dx = 0;
  boss->fall_dy = 0;
  boss->xspeed = 0;
  boss->yspeed = 0;

  boss->acu_anim = -1;
  boss->show_acu = -1;
}

void boss_collision(struct boss *boss, struct agent *collider)
{
  if (collider->meta->elt == BLOCK)
  {
    if (boss->state == BOSS_JUMP)
    {
      boss->rect.y += 10;
      SWITCH_STATE(DEFLG, deflg);
    }
    else if (boss->state == BOSS_FALL)
    {
      boss->rect.y -= 10;
      SWITCH_STATE(IDLE, idle);
    }
    else
    {
      if (boss->rect.x * 32 > 58)
        boss->rect.x -= 5;
      boss->rect.y -= 5;
    }
  }

  else if (collider->meta->elt == PLAYER && boss->state == BOSS_FALL
           && !boss->player_got_it && !player_get()->isgod)
  {
    player_get()->health--;
    boss->player_got_it = 1;
  }

  else if (collider->meta->elt == PLAYER_ATTACK && !boss->i_got_it)
  {
    --boss->health;
    boss->i_got_it = 1;
    if (boss->health <= 0)
    {
      boss->health = 0;
      SWITCH_STATE(DEAD, dead);
    }
  }
}

static void boss_shoot(struct boss *boss)
{
  struct fireball *f1 = fireball_make();
  f1->xspeed = 15 * boss_rel_player_pos(boss);
  f1->yspeed = 0;
  f1->rect.x = boss->rect.x + boss->rect.w - 35;
  f1->rect.y = boss->rect.y + 45;
  fireball_init(f1);
  engine_agent_add((struct agent *)f1);
}

static void boss_deflg(struct boss *boss)
{
  for (int dy = -1; dy <= 1; ++dy)
  {
    for (int dx = -1; dx <= 1; ++dx)
    {
      if (!(dx == 0 && dy == 0))
      {
        struct fireball *f1 = fireball_make();
        f1->xspeed = 10 * dx;
        f1->yspeed = 10 * dy;
        f1->rect.x = boss->rect.x + boss->rect.w / 2;
        f1->rect.y = boss->rect.y + boss->rect.h / 2;
        fireball_init(f1);
        engine_agent_add((struct agent *)f1);
      }
    }
  }
}

static void compute_fall_angle(struct boss *boss)
{
  SDL_Rect pl = player_get()->rect;
  double fall_dist = sqrt(pow(boss->rect.x - pl.x, 2)
                          + pow(boss->rect.y - pl.y, 2));
  double adj = sqrt(pow(boss->rect.x - pl.x, 2)
                    + pow(boss->rect.y - boss->rect.y, 2));

  double fall_angle = acos(adj / fall_dist);
  boss->fall_dx = fall_dist * cos(fall_angle) * boss_rel_player_pos(boss);
  boss->fall_dy = fall_dist * sin(fall_angle);
}

void boss_update(struct boss *boss, double delta)
{
  boss->state_duration += delta;
  if (boss->show_acu >= 0)
    boss->acu_anim += delta;

  if (boss->state == BOSS_SHOOT && boss->state_duration >= boss->tick / 2)
    boss_shoot(boss);

  if (boss->state == BOSS_DEFLG && 3 <= boss->clip_idx && boss->clip_idx <= 5)
    boss_deflg(boss);

  if (boss->state == BOSS_JUMP)
    boss->rect.y -= 10;

  if (boss->state == BOSS_FALL)
  {
    boss->rect.x += boss->fall_dx / 50.0;
    boss->rect.y += boss->fall_dy / 50.0;
  }

  if (boss->state_duration >= boss->tick)
  {
    boss->state_duration = 0;
    boss->clip_idx = (boss->clip_idx + 1) % boss->clip_cnt;

    if (boss->clip_idx == 0)
    {
      switch (boss->state)
      {
      case BOSS_SCREAM:
        SWITCH_STATE(SHOOT, shoot);
        break;
      case BOSS_SHOOT:
        SWITCH_STATE(PREPARE_JUMP, prepare_jump);
        break;
      case BOSS_PREPARE_JUMP:
        SWITCH_STATE(JUMP, jump);
        break;
      case BOSS_DEFLG:
        compute_fall_angle(boss);
        boss->player_got_it = 0;
        SWITCH_STATE(FALL, fall);
        break;
      case BOSS_IDLE:
        SWITCH_STATE(SHOOT, shoot);
        break;
      case BOSS_DEAD:
        if (boss->scale <= 1)
        {
          if (boss->acu_anim == -1)
          {
            boss->acu_anim = 0.0;
            boss->show_acu = 1;
          }
        }
        else
          boss->scale = boss->scale - 0.2;
        break;
      default:
        break;
      }
    }
  }
}

void boss_draw(struct boss *boss)
{
  struct engine *engine = engine_get();
  SDL_Rect pos =
  {
    .x = boss->rect.x - engine->scene->view.x,
    .y = boss->rect.y - engine->scene->view.y,
    .w = boss->clips[boss->clip_idx].w / boss->scale,
    .h = boss->clips[boss->clip_idx].h / boss->scale
  };

  if (boss->state == BOSS_FALL)
  {
    pos.w *= 1.4;
    pos.h *= 1.4;
  }

  SDL_Rect hbar =
  {
    .x = pos.x - 16,
    .y = pos.y - 15,
    .w = 128.0 * 1.25,
    .h = 10
  };

  if (pos.h > 128)
    pos.y -= (pos.h - 128);
  else
    pos.y += (128 - pos.h);
  pos.x -= (pos.w - 128) / 2;

  draw_health_bar(hbar, boss->health / HEALTH_MAX);

  if (boss->state == BOSS_SHOOT)
  {
    SDL_RendererFlip flip = (SDL_FLIP_HORIZONTAL
                             & (boss_rel_player_pos(boss) == 1));
    SDL_RenderCopyEx(engine->renderer, textures_get(boss->meta->elt),
                     &boss->clips[boss->clip_idx], &pos, 0, NULL, flip);
  }
  else
  {
    SDL_RenderCopy(engine->renderer, textures_get(boss->meta->elt),
                   &boss->clips[boss->clip_idx], &pos);
  }

  if (boss->show_acu > 0)
  {
    int has_photo = 0;
    for (int i = 0; i < NB_PHOTO; ++i)
    {
      if (score_get()->photos[i])
      {
        has_photo = 1;
        SDL_Rect pos = get_acu_pos(i, boss);
        SDL_RenderCopy(engine->renderer, textures_get(PHOTO + i), NULL, &pos);
      }
    }
    if (!has_photo)
      boss->acu_anim = 6000;
  }

  if (boss->acu_anim == 6000)
  {
    engine->next_scene = credits_scene_make();
  }
}

void boss_did_enter_scene(struct boss *boss)
{
  boss = boss;
}

void boss_destroy(struct boss *boss)
{
  free(boss);
}
