#include <stdlib.h>

#include "platfall.h"
#include "textures.h"

void platfall_init(struct platfall *platfall)
{
  platfall->rect.x = (platfall->rect.x - 1) * 32;
  platfall->rect.y *= 32;
  platfall->rect.w = 96;
  platfall->rect.h = 32;

  platfall->yspeed = 0;
  platfall->gravity = 0.f;
}

void platfall_collision(struct platfall *platfall, struct agent *collider)
{
  if (collider->meta->elt == PLAYER)
    platfall->gravity = 1.f;
  else if (platfall->gravity == 1.f)
    engine_agent_remove(agent_get(platfall));
}

void platfall_update(struct platfall *platfall, double delta)
{
  delta = delta;
  platfall->rect.y += platfall->yspeed;
  platfall->yspeed += platfall->gravity;
}

void platfall_did_enter_scene(struct platfall *platfall)
{
  platfall = platfall;
}

void platfall_draw(struct platfall *platfall)
{
  struct engine *engine = engine_get();
  SDL_Rect pos =
  {
    .x = platfall->rect.x - engine->scene->view.x,
    .y = platfall->rect.y - engine->scene->view.y,
    .w = platfall->rect.w,
    .h = platfall->rect.h
  };
  SDL_RenderCopy(engine_get()->renderer, textures_get(platfall->meta->elt),
                 NULL, &pos);
}

void platfall_destroy(struct platfall *platfall)
{
  free(platfall);
}
