#include <stdlib.h>

#include "fireball.h"
#include "player_attack.h"
#include "textures.h"

void player_attack_init(struct player_attack *player_attack)
{
  player_attack->rect.w = 70;
  player_attack->rect.h = 70;
}

void player_attack_collision(struct player_attack *player_attack,
                             struct agent *collider)
{
  player_attack = player_attack;
  if (collider->meta->elt == FIREBALL)
  {
    void *tmp = collider;
    struct fireball *fb = tmp;
    fb->xspeed = 0;
    fb->yspeed = 0;
  }
}

void player_attack_update(struct player_attack *player_attack, double delta)
{
  player_attack->direction = player_attack->player->direction;
  if (!player_attack->direction)
  {
    player_attack->rect.x = player_attack->player->rect.x - 32;
  }
  else
    player_attack->rect.x = player_attack->player->rect.x + 32;
  player_attack->rect.y = player_attack->player->rect.y - 4;

  ++player_attack->image;
  if (player_attack->image > 17)
    engine_agent_remove(agent_get(player_attack));
  delta = delta;
}

void player_attack_did_enter_scene(struct player_attack *player_attack)
{
  player_attack = player_attack;
}

void player_attack_draw(struct player_attack *player_attack)
{
  struct engine *engine = engine_get();
  SDL_Rect pos =
  {
    .x = player_attack->rect.x - engine->scene->view.x,
    .y = player_attack->rect.y - engine->scene->view.y,
    .w = player_attack->rect.w,
    .h = player_attack->rect.h
  };
  SDL_RendererFlip flip = SDL_FLIP_HORIZONTAL & !player_attack->direction;

  SDL_RenderCopyEx(engine_get()->renderer,
      textures_get(player_attack->meta->elt + player_attack->image / 2),
      NULL, &pos, 0, NULL, flip);
}

void player_attack_destroy(struct player_attack *player_attack)
{
  free(player_attack);
}
