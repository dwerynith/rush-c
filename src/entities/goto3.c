#include <stdlib.h>

#include "engine.h"
#include "goto3.h"
#include "photos.h"
#include "scenes.h"
#include "textures.h"

void goto3_init(struct goto3 *goto3)
{
  goto3->rect.x *= 32;
  goto3->rect.y *= 32;
  goto3->rect.w = 32;
  goto3->rect.h = 160;
  goto3->pos = 0;
}

void goto3_collision(struct goto3 *goto3, struct agent *collider)
{
  goto3 = goto3;
  if (collider->meta->elt == PLAYER)
  {
    struct scene *boss_scene = boss_scene_make();
    engine_agent_remove(collider);
    engine_get()->next_scene = boss_scene;
  }
}

void goto3_update(struct goto3 *goto3, double delta)
{
  goto3 = goto3;
  delta = delta;
}

void goto3_did_enter_scene(struct goto3 *goto3)
{
  goto3 = goto3;
}

void goto3_draw(struct goto3 *goto3)
{
  SDL_RendererFlip flip = SDL_FLIP_HORIZONTAL;
  SDL_RenderCopyEx(engine_get()->renderer, textures_get(goto3->meta->elt),
                 NULL, &goto3->rect, 0, NULL, flip);
}

void goto3_destroy(struct goto3 *goto3)
{
  free(goto3);
}
