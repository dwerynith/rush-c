#include <stdlib.h>

#include "block.h"
#include "textures.h"

void block_init(struct block *block)
{
  block->rect.x *= 32;
  block->rect.y *= 32;
  block->rect.w = 32;
  block->rect.h = 32;
}

void block_collision(struct block *block, struct agent *collider)
{
    block = block;
    collider = collider;
}

void block_update(struct block *block, double delta)
{
    block = block;
    delta = delta;
}

void block_did_enter_scene(struct block *block)
{
    block = block;
}

void block_draw(struct block *block)
{
  struct engine *engine = engine_get();
  SDL_Rect pos =
  {
    .x = block->rect.x - engine->scene->view.x,
    .y = block->rect.y - engine->scene->view.y,
    .w = block->rect.w,
    .h = block->rect.h
  };
  SDL_RenderCopy(engine_get()->renderer, textures_get(block->meta->elt),
                 NULL, &pos);
}

void block_destroy(struct block *block)
{
    free(block);
}
