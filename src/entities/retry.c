#include <stdlib.h>

#include "engine.h"
#include "retry.h"
#include "photos.h"
#include "scenes.h"
#include "textures.h"

void retry_init(struct retry *retry)
{
  retry->rect.x *= 32;
  retry->rect.y *= 32;
  retry->rect.w = 32;
  retry->rect.h = 160;
  retry->pos = 0;
}

void retry_collision(struct retry *retry, struct agent *collider)
{
  retry = retry;
  if (collider->meta->elt == PLAYER)
  {
    struct scene *game_over_scene = first_scene();
    engine_agent_remove(collider);
    engine_get()->next_scene = game_over_scene;
  }
}

void retry_update(struct retry *retry, double delta)
{
  retry = retry;
  delta = delta;
}

void retry_did_enter_scene(struct retry *retry)
{
  retry = retry;
}

void retry_draw(struct retry *retry)
{
  SDL_RendererFlip flip = SDL_FLIP_HORIZONTAL;
  SDL_RenderCopyEx(engine_get()->renderer, textures_get(retry->meta->elt),
                 NULL, &retry->rect, 0, NULL, flip);
}

void retry_destroy(struct retry *retry)
{
  free(retry);
}
