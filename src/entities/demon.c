#include <stdlib.h>
#include <string.h>

#include "demon.h"
#include "player_attack.h"
#include "fireball.h"
#include "textures.h"
#include "health_bar.h"

void demon_init(struct demon *demon)
{
  demon->rect.x *= 32;
  demon->rect.y *= 32;
  demon->rect.w = 32;
  demon->rect.h = 32;

  demon->direction = 1;

  demon->xspeed = 2;
  demon->yspeed = 0;
  demon->shoot = COOLDOWN;

  demon->damaged = 0;
  demon->hp = 2;
}

void demon_collision(struct demon *demon, struct agent *collider)
{
    if (collider->meta->elt == BLOCK)
    {
        demon->direction = -demon->direction;
        demon->rect.x += demon->direction * 10;
    }
    else if (collider->meta->elt == PLAYER_ATTACK
             && collider->uid != demon->damaged)
    {
      void *tmp = collider;
      struct player_attack *pa= tmp;
      demon->hp -= 1;
      if (demon->hp < 1)
      {
        ++pa->player->health;
        if (pa->player->health > 4)
            pa->player->health = 4;
      }
      demon->damaged = collider->uid;
    }
    else if (collider->meta->elt == DEMON)
    {
        void *tmp = collider;
        struct demon *d = tmp;
        demon->direction = -demon->direction;
        demon->rect.x += demon->direction * 10;
        d->direction = -d->direction;
        d->rect.x += d->direction * 10;
    }
}

static void demon_shoot(struct demon *demon)
{
  struct fireball *f1 = fireball_make();
  struct fireball *f2 = fireball_make();
  struct fireball *f3 = fireball_make();

  f1->xspeed = 1;
  f1->yspeed = 1;

  f2->xspeed = 0;
  f2->yspeed = 1;

  f3->xspeed = -1;
  f3->yspeed = 1;

  f1->rect.x = demon->rect.x;
  f1->rect.y = demon->rect.y;
  fireball_init(f1);
  engine_agent_add(agent_get(f1));

  f2->rect.x = demon->rect.x;
  f2->rect.y = demon->rect.y;
  fireball_init(f2);
  engine_agent_add(agent_get(f2));

  f3->rect.x = demon->rect.x;
  f3->rect.y = demon->rect.y;
  fireball_init(f3);
  engine_agent_add(agent_get(f3));
}

void demon_update(struct demon *demon, double delta)
{
  if (demon->hp < 1)
  {
    engine_agent_remove(agent_get(demon));
    return;
  }
  delta = delta;
  if (!demon->shoot)
  {
    demon_shoot(demon);
    demon->shoot = COOLDOWN;
  }

  demon->shoot -= 1;

  demon->rect.x += demon->direction * demon->xspeed;
}

void demon_did_enter_scene(struct demon *demon)
{
  demon = demon;
}

void demon_draw(struct demon *demon)
{
  SDL_RendererFlip flip = SDL_FLIP_HORIZONTAL & (1 == demon->direction);

  struct engine *engine = engine_get();
  SDL_Rect pos =
  {
    .x = demon->rect.x - engine->scene->view.x,
    .y = demon->rect.y - engine->scene->view.y,
    .w = demon->rect.w,
    .h = demon->rect.h
  };
  SDL_RenderCopyEx(engine_get()->renderer, textures_get(demon->meta->elt),
                 NULL, &pos, 0, NULL, flip);

  pos.h = 5;
  pos.y -= 10;
  pos.x -= 0.15 * pos.w;
  pos.w *= 1.25;
  draw_health_bar(pos, (1.0 * demon->hp) / 2.0);
}

void demon_destroy(struct demon *demon)
{
  free(demon);
}
