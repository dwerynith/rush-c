#include <SDL2/SDL.h>

#include "engine.h"
#include "health_bar.h"

void draw_health_bar(SDL_Rect bar, double health)
{
  SDL_Renderer *renderer = engine_get()->renderer;
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  SDL_RenderFillRect(renderer, &bar);

  bar.w *= health;
  if (health >= 0.99)
    SDL_SetRenderDrawColor(renderer, 128, 0, 0, 255);
  else if (health >= 0.66)
    SDL_SetRenderDrawColor(renderer, 160, 0, 0, 255);
  else
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
  SDL_RenderFillRect(renderer, &bar);
}
