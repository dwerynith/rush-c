#include "scenes.h"
#include "score.h"
#include "maps.h"

void credits_scene_init(struct scene *scene)
{
  scene->view.w = ENGINE_WIN_WIDTH;
  scene->view.h = ENGINE_WIN_HEIGHT;
  map_load("../maps/credits.map");
}

void credits_scene_destroy(struct scene *scene)
{
  free(scene);
}

struct scene *credits_scene_make(void)
{
  struct scene *s = engine_scene_make();

  score_display();
  s->init = credits_scene_init;
  s->destroy = credits_scene_destroy;

  return s;
}
