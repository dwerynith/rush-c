#include "scenes.h"
#include "maps.h"

void boss_scene_init(struct scene *scene)
{
  scene->view.w = ENGINE_WIN_WIDTH;
  scene->view.h = ENGINE_WIN_HEIGHT;
  map_load("../maps/boss.map");
}

void boss_scene_destroy(struct scene *scene)
{
  free(scene);
}

struct scene *boss_scene_make(void)
{
    struct scene *s = engine_scene_make();
    s->init = boss_scene_init;
    s->destroy = boss_scene_destroy;

    return s;
}
