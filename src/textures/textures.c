#include <stddef.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>

#include "elements.h"
#include "engine.h"
#include "textures.h"

static char **get_texture_files(void)
{
  static char *files[NB_ELEMENTS] =
  {
    NULL, "environment/block.png",
    "environment/water.png", "player/movements/standing0.png",
    "player/movements/standing1.png", "player/movements/standing2.png",
    "player/movements/standing3.png", "player/movements/standing4.png",
    "player/movements/standing5.png", "player/movements/standing6.png",
    "player/movements/standing7.png", "player/movements/standing8.png",
    "player/movements/running0.png", "player/movements/running1.png",
    "player/movements/running2.png", "player/movements/running3.png",
    "player/movements/running4.png", "player/movements/running5.png",
    "player/movements/attack0.png", "player/movements/attack1.png",
    "player/movements/attack2.png", "player/movements/attack3.png",
    "player/movements/attack4.png", "player/movements/attack5.png",
    "player/attacks/player_attack0.png", "player/attacks/player_attack1.png",
    "player/attacks/player_attack2.png", "player/attacks/player_attack3.png",
    "player/attacks/player_attack4.png", "player/attacks/player_attack5.png",
    "player/attacks/player_attack6.png", "player/attacks/player_attack7.png",
    "player/attacks/player_attack8.png", "enemies/demon.png",
    "enemies/fireball.png", "environment/platfall.png",
    "environment/spikes.png", "environment/orb.png",
    "environment/door.png", "environment/door.png",
    "environment/door.png", "environment/door.png",
    "enemies/boss.png", "photos/marque_o.png",
    "photos/banet_l.png", "photos/caster_d.png",
    "photos/cetre_c.png", "photos/chabro_a.png",
    "photos/douill_a.png", "photos/downey_j.png",
    "photos/gardet_p.png", "photos/liotie_t.png",
    "photos/lugand_j.png", "photos/marque_o.png",
    "photos/moutou_s.png", "photos/orfila_c.png",
    "photos/peicho_d.png", "photos/petit_s.png",
    "photos/rybyns_a.png", "photos/notfound.png"
  };

  return files;
}

static SDL_Texture **textures_get_all(void)
{
  static SDL_Texture *textures[NB_ELEMENTS] =
  {
    NULL
  };

  return textures;
}

SDL_Texture *textures_get(enum elements elt)
{
  SDL_Texture **textures = textures_get_all();
  return textures[elt];
}

void textures_init_all(void)
{
  char *path = "../sprites/";
  char buffer[2048] =
  {
    0
  };
  strcpy(buffer, path);

  char **files = get_texture_files();
  SDL_Texture **textures = textures_get_all();
  for (size_t i = 1; i < NB_ELEMENTS; i++)
  {
    sprintf(buffer, "%s%s", path, files[i]);

    SDL_Surface *surf = IMG_Load(buffer);
    textures[i] = SDL_CreateTextureFromSurface(engine_get()->renderer, surf);
    SDL_FreeSurface(surf);
  }
}

void textures_destroy_all(void)
{
  SDL_Texture **textures = textures_get_all();
  for (size_t i = 1; i < NB_ELEMENTS; i++)
    SDL_DestroyTexture(textures[i]);
}

struct agent *agent_get(void *agent)
{
    return agent;
}
